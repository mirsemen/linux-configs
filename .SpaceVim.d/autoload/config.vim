function! config#before() abort
endfunction

function! config#after() abort
	let &t_TI = "\<Esc>[>4;2m"
	let &t_TE = "\<Esc>[>4;m"
	set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz
    set makeprg=ninja\ -C\ build\ -k\ 3
	set guifont=Hack\ Nerd\ Font\ Mono:h20
	set termguicolors
    set cc=80
    set foldmethod=marker
    set foldlevel=99
    set foldcolumn=2
	set foldmarker={_{_{,}_}_}
	set signcolumn=yes
    set cpoptions+=$

    let g:neomake_c_enabled_makers = ['clangtidy']
    let g:neomake_cpp_enabled_makers = ['clangtidy']
	let g:tagbar_autofocus = 1
	" tpope vimsurround plugin config: visual mode: Sf. 102='f'
	let g:surround_102 = "// clang-format off\n\r\n// clang-format on"
	let g:smoothie_enabled = 0

	nnoremap <silent> <F3> :Defx -winwidth=45<CR>
	map <silent> <F4> :A <CR>
    map <silent> <F9> :wa<CR>:make<CR>
	nmap <Tab> <C-W><C-W>
	nmap <F5> gd
	nmap <C-S> :wa<CR>
    nmap <leader>n :noh<CR>
	nmap <leader>m :Neomake clangtidy<CR>
	nmap <leader>c <Space>bd
	nnoremap <leader>s :%s/\<<C-r><C-w>\>//g<Left><Left>
	vnoremap <leader>sc cstatic_cast<>()<Esc>PF<a
	vnoremap <leader>se y:.,.s/\<<C-r>0\>/\/*\0*\//<CR>

    noremap <Up> <NOP>
    noremap <Down> <NOP>
    noremap <Left> <NOP>
    noremap <Right> <NOP>
    vnoremap <Up> <NOP>
    vnoremap <Down> <NOP>
    vnoremap <Left> <NOP>
    vnoremap <Right> <NOP>
    inoremap <Up> <NOP>
    inoremap <Down> <NOP>
    inoremap <Left> <NOP>
    inoremap <Right> <NOP>

    nnoremap <PageUp> <NOP>
    inoremap <PageUp> <NOP>
    nnoremap <PageDown> <NOP>
    inoremap <PageDown> <NOP>
    nnoremap <Home> <NOP>
    inoremap <Home> <NOP>
    nnoremap <End> <NOP>
    inoremap <End> <NOP>

    augroup quickfix
        autocmd!
        autocmd FileType qf setlocal wrap
    augroup END


	if (has('nvim'))
lua << EOF
require 'nvim-treesitter.configs'.setup {
	ensure_installed = {"cpp", "c"},
	highlight = {
		enable = true,
	},
	incremental_selection = {
		enable = true,
	},
	indent = {
		enable = true,
	}
}
EOF
	endif

endfunction

