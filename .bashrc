# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	# echo "Non interactive. Bye!"
	# sleep 1
	return
fi

source /etc/profile

# Put your fun stuff here.
export HISTCONTROL=ignoreboth
export CCACHE_DIR=/tmp/$USER/.ccache
export PATH="/home/sem/bin:$HOME/perl5/bin:$HOME/.local/bin:$HOME/.cargo/bin:/usr/lib/ccache/bin:$PATH"
export PKG_CONFIG_PATH="/usr/lib64/pkgconfig:/usr/local/lib64/pkgconfig:/usr/lib/pkgconfig:/usr/local/lib/pkgconfig:/usr/share/pkgconfig:/usr/local/share/pkgconfig"

export QT_QPA_PLATFORMTHEME=qt5ct

export PERL5LIB="$HOME/perl5/lib/perl5:$PERL5LIB"
export PERL_LOCAL_LIB_ROOT="$HOME/"
export PERL_MB_OPT="--install_base \"$HOME/perl5\""
export PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"

export MANWIDTH=80
export MANPATH="$HOME/.local/share/man:$MANPATH"
export EDITOR=/usr/bin/nvim
export MANPAGER=most

export NVIM_LISTEN_ADDRESS=/tmp/nvimsocket
# export SWAYSOCK=`ls /run/user/1000/sway-ipc.* | head -n 1`

export SUDO_ASKPASS="/usr/bin/x11-ssh-askpass"

export NO_AT_BRIDGE=1

export WINEARCH=win32

source /usr/share/git/git-prompt.sh
[ -f ~/.bash_alias ] && source ~/.bash_alias

# use after aliases enabled
help() {
    "$@" --help 2>&1 | bathelp
}

if [[ $(whoami) = "root" ]]; then
	export PS1="\[\033]0;\u@\h:\w\007\]\[\033[01;31m\]\u@\h\[\033[01;34m\] \w\$(__git_ps1)$\[\033[00m\] "
else
	if [ -f ~/.bash_helpers ]; then
		source ~/.bash_helpers
		export PS1="\[\033]0;\u@\h:\w\007\]\[\033[01;32m\]\u@\h\[\033[01;34m\] \w\$(__git_ps1)$\[\033[00m\] "
	else
		export PS1="\[\033]0;\u@\h:\w\007\]\[\033[01;32m\]\u@\h\[\033[01;34m\] \w$\[\033[00m\] "
	fi
fi

export TERM=xterm-256color

stty -ixon # Disable ctrl-s and ctrl-q.

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
[ -x $HOME/.fehbg ] && $HOME/.fehbg

# eval `ssh-agent -s`
# keep this line at the bottom of ~/.bashrc
# fish_shell='/bin/fish'
# [ $(tty) != '/dev/tty1' ] && [ -x $fish_shell ] && SHELL=$fish_shell && exec $fish_shell
