if status --is-interactive
set fish_greeting
if test "$USER" = "root"
    set dir "/root"
else
    set dir "/home/$USER"
end

source /home/sem/.config/fish/alias.fish

if test -e mcfly
	mcfly init fish | source
end

end
