# Defined in /tmp/fish.tkWNS8/fish_prompt.fish @ line 2
function fish_prompt
    echo -n -s \
        (set_color red --bold) "[" \
        (set_color blue) "$USER" \
        (set_color green) "@" \
        (set_color magenta) "$PWD" \
        (set_color red --bold) "]" \
        (set_color $fish_color_command)
    # set_color red --bold
    # printf "["
    # set_color yellow
    # printf "%s" "$USER"
    # set_color green
    # printf "@"
    # set_color magenta
    # printf "%s" "$PWD"
    # set_color red
    # printf "]"
    # set_color normal
end
