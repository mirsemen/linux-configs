# Defined in /tmp/fish.jvyWk9/ls.fish @ line 2
function ls --description 'List contents of directory'
    

    # BSD, macOS and others support colors with ls -G.
    # GNU ls and FreeBSD ls takes --color=auto. Order of this test is important because ls also takes -G but it has a different meaning.
    # Solaris 11's ls command takes a --color flag.
    # Also test -F because we'll want to define this function even with an ls that can't do colors (like NetBSD).
    if not set -q __fish_ls_color_opt
        set -g __fish_ls_color_opt
        for opt in --color=auto -G --color -F
            if command lsd $opt / >/dev/null 2>/dev/null
                set -g __fish_ls_color_opt $opt
                break
            end
        end
    end

    # Set the colors to the default via `dircolors` if none is given.
    # __fish_set_lscolors

    isatty stdout
    and set -a opt -F
    command lsd --date +%Y-%m-%d\ %H:%M:%S $__fish_ls_color_opt $argv
end
