# Defined in /tmp/fish.2t5rZ7/mnt.fish @ line 2
function mnt
  mount | grep -vE 'type tmpfs|overlaid|on /sys|on /dev|on /boot|on /var/lib|on /run/user|on /proc' | sed "s/\(.*\) on \(.*\) type \(.*\) \(.*\)/\1 \2 \3 \4/" | column --table --table-columns SOURCE,TARGET,TYPE,OPTIONS --table-wrap OPTIONS $argv;
end
