function mntro
    set disk (mount | grep '$1' | awk '{print $1}')
    umount $argv
    mount -o ro $disk $argv
end
