function mntrw
    set disk (mount | grep '$1' | awk '{print $1}')
    umount $argv
    mount -o rw $disk $argv
end
