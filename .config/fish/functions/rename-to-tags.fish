function rename-to-tags
    for f in (ls -1 *.flac)
        set -l artist (metaflac --show-tag=artist $f | awk -F= '{print $2}')
        set -l no (metaflac --show-tag=tracknumber $f | awk -F= '{printf("%.2d", $2)}')
        set -l title (metaflac --show-tag=title $f | awk -F= '{print $2}')
        mv $f "$no. $artist - $title.flac"
    end
end
