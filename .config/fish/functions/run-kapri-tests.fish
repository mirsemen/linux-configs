function run-kapri-tests
    while true
        set -l prev (stat -c %W ./build/test/test-doctest)
        while true
            if test -x ./build/test/test-doctest
                set -l now (stat -c %W ./build/test/test-doctest)
                if test $prev -ne $now
                    #echo Run test
                    ./build/test/test-doctest -ne -nv -nt
					echo (date) "=== TEST === DONE ==="
                    set prev $now
                else
                    sleep 1
                end
            else
                sleep 1
            end
        end
    end
end
