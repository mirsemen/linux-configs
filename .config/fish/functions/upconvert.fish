# Defined in /tmp/fish.KaSXWQ/upconvert.fish @ line 1
function upconvert
	for f in (ls -1 *.flac );
		set -l dir (metaflac --show-sample-rate $f)
		set -l dir2 (math "$dir * 2");
		mkdir -p $dir2;
		echo $f...
		sox --multi-threaded -SGq "$f" "$dir2/$f" rate -vsM $dir2;
	end
end
