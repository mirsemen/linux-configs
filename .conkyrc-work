-- vim: ts=4 sw=4 noet ai cindent syntax=lua
conky.config = {
alignment = 'top_right',
background = false,
border_width = 5,
color1 = '07CC0D',
color2 = 'D1E7D1',
color3 = 'FF0000',
color4 = 'FFFFFF',
cpu_avg_samples = 2,
default_color = 'D1E7D1',
default_outline_color = 'white',
default_shade_color = 'white',
double_buffer = true,
draw_borders = false,
draw_graph_borders = true,
draw_outline = false,
draw_shades = false,
border_outer_margin = 0,
format_human_readable = true,
max_port_monitor_connections = 64,
maximum_width = 600,
minimum_width = 600,
max_user_text = 16384,
net_avg_samples = 2,
no_buffers = yes,
out_to_console = no,
stippled_borders = 2,
own_window = true,
own_window_class = 'Conky',
own_window_type = 'normal',
own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
own_window_transparent = true,
own_window_argb_visual = true,
own_window_argb_value = 40,
update_interval = 0.5,
uppercase = false,
use_xft = true,
xftalpha = 0,
--font = 'Bitstream Vera Sans Mono:size=12'
font = 'Dejavu Sans Mono:size=14',
template0 = [[${goto 40}\1${goto 100}|${goto 120}\2${goto 200}|${goto 210}\3${goto 300}|${goto 325}\4${goto 400}|${goto 420}\5${goto 500}|${goto 525}\6]],
template1 = [[${color \1}\2${goto 100}|${fs_used \3}${goto 200}|${fs_free \3}${goto 300}|${fs_size \3}${goto 400}|${diskio_read \4}${goto 500}|${diskio_write \4}
${fs_bar 5 \3}\
]],
template2 = [[${color \1}\2${goto 100}|${fs_used \3}${goto 200}|${fs_free \3}${goto 300}|${fs_size \3}${goto 400}|${goto 500}|
${fs_bar 5 \3}\
]],
template3 = [[${color \1}\2${goto 100}|$swap${goto 200}|$swapfree${goto 300}|$swapmax${goto 400}|${diskio_read \3}${goto 500}|${diskio_write \3}
${color #A0cc55}${swapbar 5 color1 color3}\
]]
}

conky.text = [[
${color1}Пользователь: ${color}$user_names $nodename ${alignr}${color1}Ядро: ${color}$kernel  
${color1}Uptime:${color} $uptime ${color1} ${alignr}Загрузка:${color} $loadavg 
${voffset 15}${alignc}${font :size=42}${voffset -20}${color #00afff}${time %d.%m.%Y}   ${color #f000ff}${time %H:%M}${font}
${font :size=18}${color #00ffef}${alignc}${time %A}${font}
${color1}$hr
${alignr}${color1}Память: $memperc% = $mem / $memmax
${membar}
${color #88cc00}${cpugraph cpu0 60,290 07CC0D FF0000 -t} ${goto 315}${color #88cc00}${memgraph 60,290 07CC0D FF0000 -t}
${color}ЦПУ 1: ${color1}${cpu cpu1}% ${goto 140}${cpubar cpu1 12,150} ${goto 315}${color}ЦПУ 5: ${color1}${cpu cpu5}% ${alignr}${cpubar cpu5 12,150}
${color}ЦПУ 2: ${color1}${cpu cpu2}% ${goto 140}${cpubar cpu2 12,150} ${goto 315}${color}ЦПУ 6: ${color1}${cpu cpu6}% ${alignr}${cpubar cpu6 12,150}
${color}ЦПУ 3: ${color1}${cpu cpu3}% ${goto 140}${cpubar cpu3 12,150} ${goto 315}${color}ЦПУ 7: ${color1}${cpu cpu7}% ${alignr}${cpubar cpu7 12,150}
${color}ЦПУ 4: ${color1}${cpu cpu4}% ${goto 140}${cpubar cpu4 12,150} ${goto 315}${color}ЦПУ 8: ${color1}${cpu cpu8}% ${alignr}${cpubar cpu8 12,150}
$hr
${color #d0df00}${alignc}Датчики:
${color1}Температура °С:
#${color1}HDDs  ${color} ${hddtemp /dev/sda}    ${hddtemp /dev/sdb}    ${hddtemp /dev/sdc}    ${hddtemp /dev/sdd}    ${hddtemp /dev/sde}
${color1}Ядра      ${color} ${hwmon 0 temp 2}      ${hwmon 0 temp 3}      ${hwmon 0 temp 4}      ${hwmon 0 temp 5}
${color1}Плата     ${color} ${hwmon 1 temp 2}
${color1}Скорость вентиляторов (об/мин):
${color1}Процессор ${color} ${hwmon 1 fan 2}     ${color1}Плата   ${color}${hwmon 1 fan 5}
${color1}$hr
${color #d0df00}${alignc}Файловые системы:

${template0 ФС Занято Свободно Всего Чтение Запись}
${color1}$hr
${template1 #ff7a7a ROOT / /dev/disk/by-uuid/1b633e20-8dc0-4925-8f2e-5f5e82ad6440}\
${template1 #00afd0 HOME /home /dev/disk/by-uuid/a778dbdc-1231-4f06-874d-c5d2fb1d5291}\
${template2 #f57900 /v/c/tmp /var/calculate/tmp}\
${template3 #edd400 Своп /dev/disk/by-uuid/04e80677-d439-42e9-aa68-90f3b58d8c56}\
${if_mounted /media/Data}\
${template1 #73d216 Data /media/Data /dev/disk/by-label/Data}\
${endif}\
${if_mounted /run/media/sem/ssd-ntfs}\
${template1 #dc74fc ssd-ntfs /media/ssd-ntfs /dev/disk/by-label/ssd-ntfs}\
${endif}
${color #d0df00}${alignc}Сеть:

${color1}${font :size=20}⬇ D${font}${voffset -4}: ${color}${totaldown eth0} / ${downspeed eth0}${goto 300}${voffset -3}\
${color1}${font :size=20}⬆ U${font}${voffset -4}: ${color}${totalup eth0} / ${upspeed eth0}
]]
