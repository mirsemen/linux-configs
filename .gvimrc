set guioptions-=T  " no toolbar
set lines=40 columns=140
set linespace=0
" set cuc
set guifont=DejaVu\ Sans\ Mono\ 14
" Settings for MacVim and Inconsolata font
let g:CtrlSpaceSymbols = { "File": "◯", "CTab": "▣", "Tabs": "▢" }

