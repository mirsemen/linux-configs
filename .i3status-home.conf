general {
	output_format = "i3bar"
	colors = true
	interval = 5
}

order += "pomodoro"
order += "loadavg"
order += "yandexdisk_status"
order += "lm_sensors"
order += "net_rate"
order += "netdata"
order += "xkb_input"
order += "tztime date"
order += "tztime time"
order += "volume_status master"

xkb_input {
  format_input = "[\?soft ][\?color=s {s}[ {v}]]"
  inputs = [{"identifier": "7247:2:SIGMACH1P_USB_Keykoard"}]
  cache_timeout = 1
}

pomodoro {
	format = "{mmss} {bar}"
	format_active = 'Погнали! №{breakno} {format}'
	format_break  = 'Перерыв №{breakno} {format}'
	format_break_stopped = 'Перерыв №{breakno} {format}'
	format_stopped = 'Готов? {format}'
	color_bad = '#6f6f6f'
	sound_pomodoro_start = "/home/sem/distrib/beep-06.mp3"
	sound_pomodoro_end   = "/home/sem/distrib/beep-09.mp3"
	sound_break_end	  = "/home/sem/distrib/beep-09.mp3"
}

loadavg {
	thresholds = {
	'1avg': [
		(0, '#9dd7fb'),
		(20, 'good'), (40, 'degraded'),
		(60, '#ffa500'), (80, 'bad')
	],
	'5avg': [
		(0, '#9dd7fb'),
		(20, 'good'), (40, 'degraded'),
		(60, '#ffa500'), (80, 'bad')
	],
	'15avg': [
		(0, '#9dd7fb'),
		(20, 'good'), (40, 'degraded'),
		(60, '#ffa500'), (80, 'bad')
	],
	}
	format =  '[\?color=1avg {1min}] '
	format += '[\?color=5avg {5min}] '
	format += '[\?color=15avg {15min}]'
}

yandexdisk_status {
	format = "Я.Диск: {status}"
	status_busy = "B"
	status_off = "S"
	status_on = "W"
}

tztime date {
	color = '#FFBBFF'
	format = "%Y-%m-%d, %A"
}

tztime time{
	color = '#FFFF00'
	format = "%H:%M"
}

volume_status master {
	thresholds = [
	(0, "#FF0000"),
	(10, "#FF6000"),
	(20, "#FFB000"),
	(30, "#FFFF00"),
	(40, "#00FF00"),
	(50, "#00FFFF"),
	(60, "#00C9FF"),
	(70, "#C06BFF"),
	(80, "#DF51FF"),
	(90, "#FF00FF")
	]
	device = "alsa_output.pci-0000_00_1f.3.analog-stereo"
#	format = "♪: %volume"
#	format_muted = "♪: muted (%volume)"
#	device = "default"
	mixer = "Master"
	on_click 4 = "exec amixer set Master 5%+"
	on_click 5 = "exec amixer set Master 5%-"
}

lm_sensors {
	min_length = 11
	cache_timeout = 10
	chips = ['pch_skylake-virtual-0']
	sensors = ['temp1']
	format_chip = 'CPU: {format_sensor}'
	format_sensor = '\?color=lime {input}°C'
}

netdata {
	cache_timeout = 1
	nic = enp1s0
	format = "[\?color=total T(Mb): {download}↓ {upload}↑ {total}↕]"
#	format = "LAN [\?color=down S(Kb): {down}↓ {up}↑] [\?color=total T(Mb): {download}↓ {upload}↑]"
}

net_rate {
	cache_timeout = 1
	format = "LAN [\?color=down S: {down}↓ {up}↑]"
	format_value = "[\?min_length=10 {value:.1f} {unit}]"
}
