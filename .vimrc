" vim: fdm=marker foldenable

if has('nvim')
    let plug_path = '~/.config/nvim/autoload'
else
    let plug_path = '~/.vim/autoload'
endif

let plug_file = expand(plug_path) . '/plug.vim'

if ! filereadable(plug_file)
    echo "Downloading junegunn/vim-plug to manage plugins..."
    silent execute '!mkdir -p ' . expand(plug_path)
    silent execute '!curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" -o ' . plug_file
endif

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
if has('nvim')
    call plug#begin('~/.local/share/nvim/plugged')
else
    call plug#begin('~/.vim/plugged')
endif

" {{{ Plugin list
" Plug 'craigemery/vim-autotag'
" Plug 'ctrlpvim/ctrlp.vim'
" Plug 'lervag/vimtex'
Plug 'tomtom/tcomment_vim'
Plug 'sheerun/vim-polyglot'
Plug 'Yggdroot/indentLine'
Plug 'fenetikm/falcon'
Plug 'altercation/vim-colors-solarized'
Plug 'easymotion/vim-easymotion'
Plug 'elzr/vim-json'
Plug 'godlygeek/tabular'
" Plug 'justinmk/vim-syntax-extra'
Plug 'majutsushi/tagbar'
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-vinegar'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'skywind3000/asyncrun.vim'
Plug 'tomtom/stakeholders_vim'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'valloric/youcompleteme', { 'do': './install.py --clang-completer --ninja' }
Plug 'vhdirk/vim-cmake'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-ctrlspace/vim-ctrlspace'
Plug 'vim-scripts/bash-support.vim'
Plug 'vim-syntastic/syntastic'
" }}}

" Initialize plugin system
call plug#end()

" Required by vim-ctrlspace
set nocompatible
set hidden

" Color scheme
set background=dark
colorscheme gruvbox " solarized gruvbox
" 256 color scheme
set t_Co=256
" Transparent bg
hi Normal guibg=NONE ctermbg=NONE

" Airline
set laststatus=2   " Always show the statusline
set encoding=utf-8 " Necessary to show Unicode glyphs"
set noshowmode " Hide the default mode text (e.g. -- INSERT -- below the statusline)
set smartcase " Do smart case matching
set autowrite " Automatically save before commands like :next and :make
set lazyredraw
set backspace=indent,eol,start

" Tab
" =================================
" Use spaces instead of tabs
set expandtab
" size of a hard tabstop
set tabstop=4
" size of an indent
set shiftwidth=4
" a combination of spaces and tabs are used to simulate tab stops at a width
" other than the (hard)tabstop
set softtabstop=4
" ================================

" Line numbers
set number
set relativenumber

" set textwidth=80
if exists('+colorcolumn')
    set colorcolumn=80
endif

" перенос по словам, а не по буквам
set linebreak
set dy=lastline

" Включаем подсветку выражения, которое ищется в тексте
set hlsearch
" При поиске перескакивать на найденный текст в процессе набора строки
set incsearch
" Останавливать поиск при достижении конца файла
set nowrapscan
" Игнорировать регистр букв при поиске
set ignorecase
" Отключаем создание бэкапов
set nobackup
set nowb
" Отключаем создание swap файлов
set noswapfile
set ai "Auto indent
set si "Smart indent
set nowrap "Wrap lines
" выделять строку, на которой находится курсор
set cursorline
set nocursorcolumn
set textwidth=0 wrapmargin=0
" Автоматически обновлять файлы, которые были изменены ВНЕ vim
set autoread
" Чтобы корректно распознавались ошибки при компиляции и открывался нужный
" файл
set errorformat+=%DEntering\ directory\ '%f',%XLeaving\ directory
set errorformat+=%.%#\ \(%f\\,\ line\ %l\):\ %m
set errorformat+=%E\[%f:\ error\ %n\]\ Line\ %l:\ %m,%+G

set noshowmatch " Disable highlighting pair braces

set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz
if has('nvim')
    let g:node_host_prog = '/home/sem/node_modules/.bin/neovim-node-host'
endif
" Горячие клавиши
" ==================================
" Disable arrow keys
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
vnoremap <Up> <NOP>
vnoremap <Down> <NOP>
vnoremap <Left> <NOP>
vnoremap <Right> <NOP>
inoremap <Up> <NOP>
inoremap <Down> <NOP>
inoremap <Left> <NOP>
inoremap <Right> <NOP>

nnoremap <PageUp> <NOP>
inoremap <PageUp> <NOP>
nnoremap <PageDown> <NOP>
inoremap <PageDown> <NOP>

" Tagbar show/hide
nmap <F8> :TagbarToggle<CR>

" File->save
nmap <F2> :w<CR>
imap <F2> <C-\><C-O>:w<CR>

" NERDTree
let NERDTreeIgnore=['\.pyc$']
imap <C-t> :NERDTreeToggle<CR>
nmap <C-t> :NERDTreeToggle<CR>

" комментируем строку, повторное нажатие убирает комментарий
map <C-\> <plug>NERDCommenterToggle
" новая вкладка
" map nt :tabe<CR>

" Очистить подсветку поиска (highlight clear)
map <Leader>n :noh<CR>

" Переключение между окнами (не вкладками!)
" nmap <silent> <A-Up> :wincmd k<CR>
" nmap <silent> <A-Down> :wincmd j<CR>
" nmap <silent> <A-Left> :wincmd h<CR>
" nmap <silent> <A-Right> :wincmd l<CR>

map <silent> <F9> :Make<CR>

imap <F3> :YcmCompleter GoToDefinition<CR>
nmap <F3> :YcmCompleter GoToDefinition<CR>

imap <F4> :YcmCompleter GoToDeclaration<CR>
nmap <F4> :YcmCompleter GoToDeclaration<CR>

imap <C-w>gi :YcmCompleter GoToImprecise<CR>
nmap <C-w>gi :YcmCompleter GoToImprecise<CR>

imap <C-w>gt :YcmCompleter GoTo<CR>
nmap <C-w>gt :YcmCompleter GoTo<CR>

nnoremap gf :YcmCompleter GoToInclude<CR>
" ==================================

" Enable folding
set foldmethod=marker
set foldlevel=99
set foldcolumn=2

" Fold plugin
let g:fold_options = {
   \ 'fold_blank': 0,
   \ 'fold_includes': 0,
   \ 'max_foldline_length': 'win',
   \ 'merge_comments' : 1,
   \ 'show_if_and_else': 1,
   \ 'strip_namespaces': 1,
   \ 'strip_template_arguments': 1
   \ }

" По умолчанию проверка орфографии выключена.
set spell spelllang=
set nospell

" Airline статус-линия
let g:airline_theme='gruvbox' "'base16_solarized'
let g:airline_exclude_preview = 1 " for Ctrl-Space plugin
let g:airline#extensions#ctrlspace#enabled = 1
let g:airline#extensions#ycm#enabled = 1
let g:airline#extensions#ycm#error_symbol = 'E:'
let g:airline#extensions#ycm#warning_symbol = 'W:'
" let g:airline#extensions#vimtex#enabled = 1
let g:airline#extensions#tagbar#enabled = 1
let g:airline#extensions#tagbar#flags = 's'

" Подсветка вкладок
"let g:airline#extensions#tabline#enabled = 1

" NERD Commenter
" =============================================
let g:NERDSpaceDelims            = 1 " Add spaces after comment delimiters by default
let g:NERDCompactSexyComs        = 1 " Use compact syntax for prettified multi-line comments
let g:NERDDefaultAlign           = 'left' " Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDCommentEmptyLines      = 1 " Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDTrimTrailingWhitespace = 1 " Enable trimming of trailing whitespace when uncommenting
" =============================================

" CMake
" =============================================
let g:cmake_project_generator       = 'Ninja'
let g:cmake_export_compile_commands = 1
let g:cmake_ycm_symlinks = 1
let g:cmake_cxx_compiler = 'g++'
let g:cmake_c_compiler   = 'gcc'
let g:cmake_build_type   = "Release"
" =============================================

" Custom commands
" =============================================
command! -nargs=0 SetDeb let g:cmake_build_type='Debug'
command! -nargs=0 SetRel let g:cmake_build_type='Release'

" AsyncRun
" =============================================
let g:asyncrun_open = 7
let g:asyncrun_save = 2 " Save ALL before run
let g:asyncrun_trim = 1 " Trim empty lines
command! -bang -nargs=* -complete=file Make AsyncRun ninja -C 'build/' <args> && ninja -C 'build/' <args>

" You complete me (YCM)
" =============================================
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion  = 1
let g:ycm_use_clangd = 0

" =============================================
let g:yacc_uses_cpp = 1

" Syntactic options
" =============================================
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" to use compilation database
let g:syntastic_cpp_clang_tidy_post_args = ""

" Ctrl-Space
if executable("ag")
    let g:CtrlSpaceGlobCommand = 'ag -l --nocolor -g ""'
    set grepprg=ag\ --nogroup\ --nocolor
endif
let g:CtrlSpaceSaveWorkspaceOnSwitch = 1
let g:CtrlSpaceSaveWorkspaceOnExit = 1
let g:CtrlSpaceCacheDir = expand("$HOME") . "/.vim"
let g:CtrlSpaceStatuslineFunction = "airline#extensions#ctrlspace#statusline()"
let g:CtrlSpaceUseTabline = 1
let g:CtrlSpaceSymbols = { "WLoad" : "", "WSave" : "" }
if has('nvim')
    let g:CtrlSpaceDefaultMappingKey = "<C-space> "
endif

" Indent guides
" let g:indent_guides_enable_on_vim_startup = 1
" let g:indent_guides_auto_colors = 1
" let g:indent_guides_guide_size = 1
" let g:indent_guides_start_level = 2
" let g:indent_guides_exclude_filetypes = ['help', 'nerdtree', 'tagbar']

" lh-brackets
" let g:cb_disable_default = {'(':'n', '{': 'n', '[': 'n'}

" indentLines
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

" vinegar
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'

" vim-polyglot: vim.cpp - additional vim c++ syntax highlighting
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_experimental_template_highlight = 1

