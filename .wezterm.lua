local wezterm = require 'wezterm';
return {
	default_prog = {"/bin/bash", "-i"},
	show_update_window = false,
    check_for_updates = false,
    warn_about_missing_glyphs=false,
    initial_cols = 182,
    initial_rows = 49,
    adjust_window_size_when_changing_font_size = false,
    font = wezterm.font("Hack Nerd Font Mono"),
    font_size = 14,
    scrollback_lines = 6000,
    dpi = 126.0,
    color_scheme = "Gruvbox Dark",
    colors = {
        background = "#282828",
        foreground = "#EBDBB2",
		tab_bar = {
			active_tab = {
				bg_color = "#000090",
				fg_color = "#c0c0c0",
			},
			inactive_tab = {
				bg_color = "#0b0022",
				fg_color = "#606060",
			},
		}
	},
	keys = {
		{key="y", mods="CMD", action=wezterm.action{SpawnCommandInNewTab={args={"htop", "-d2"}}}},
		{key="p", mods="CMD", action=wezterm.action{SpawnCommandInNewTab={
			args={"bash","-i"},
			cwd="/home/sem/projects/kapri/"
		}}},
    },
}
