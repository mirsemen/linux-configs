alacritty                url =  git@github.com:jwilm/alacritty.git
DirectFB                 url =  https://github.com/Distrotech/DirectFB.git
fish-shell               url =  git@github.com:fish-shell/fish-shell.git
GSL                      url =  https://github.com/Microsoft/GSL.git
highlight                url =  git@gitlab.com:saalen/highlight.git
linux-configs            url =  git@gitlab.com:mirsemen/linux-configs.git
llvm-project             url =  https://github.com/llvm/llvm-project.git
neovim-deps              url =  https://github.com/neovim/deps.git
neovim                   url =  https://github.com/neovim/neovim.git
nerd-fonts               url =  git@github.com:ryanoasis/nerd-fonts.git
qt-creator-git           url =  https://github.com/qtproject/qt-creator.git
vifm                     url =  git@github.com:vifm/vifm.git
ag-or-silver-searcher    url =  https://github.com/ggreer/the_silver_searcher.git
alacritty-theme          url =  git@github.com:eendroroy/alacritty-theme.git
bisoncpp                 url =  https://gitlab.com/fbb-git/bisoncpp.git
bobcat                   url =  https://gitlab.com/fbb-git/bobcat.git
Catch2                   url =  https://github.com/catchorg/Catch2.git
compton                  url =  https://github.com/chjj/compton.git
ctags                    url =  https://github.com/universal-ctags/ctags.git
directfb-bak             url =  https://github.com/DirectFB/directfb.git
divs-midi-utilities      url =  https://github.com/dgslomin/divs-midi-utilities.git
doctest                  url =  git@github.com:onqtam/doctest.git
draft                    url =  git@github.com:cplusplus/draft.git
extractpdfmark           url =  https://github.com/trueroad/extractpdfmark.git
flexcpp                  url =  https://gitlab.com/fbb-git/flexcpp.git
fluxcomp                 url =  https://github.com/kevleyski/fluxcomp.git
fzy                      url =  https://github.com/jhawthorn/fzy.git
googletest               url =  https://github.com/google/googletest.git
gtest-parallel           url =  https://github.com/google/gtest-parallel.git
gtest-runner             url =  https://github.com/nholthaus/gtest-runner.git
hhighlighter             url =  https://github.com/paoloantinori/hhighlighter.git
IBM-plex-fonts           url =  https://github.com/IBM/plex.git
icmake                   url =  https://gitlab.com/fbb-git/icmake.git
kernel_gcc_patch         url =  https://github.com/graysky2/kernel_gcc_patch.git
libxcb-errors            url =  https://gitlab.freedesktop.org/xorg/lib/libxcb-errors.git
libxcb-errors            url =  git://anongit.freedesktop.org/xcb/util-common-m4.git
lily                     url =  https://gitlab.com/FascinatedBox/lily.git
LilyPond-git             url =  git://git.sv.gnu.org/lilypond.git
litehtml                 url =  git@github.com:litehtml/litehtml.git
luke-dotfiles            url =  https://github.com/LukeSmithxyz/voidrice.git
MutopiaProject           url =  git@github.com:MutopiaProject/MutopiaProject.git
nlohmann-json            url =  https://github.com/nlohmann/json.git
packcc                   url =  https://github.com/enechaev/packcc.git
pywal                    url =  git@github.com:dylanaraps/pywal.git
qtcreator-solarized      url =  https://github.com/curonian/qtcreator-solarized.git
range-v3                 url =  git@github.com:ericniebler/range-v3.git
rapidjson                url =  https://github.com/Tencent/rapidjson.git
scantailor               url =  https://github.com/hkosol/scantailor.git
scdoc                    url =  https://git.sr.ht/~sircmpwn/scdoc
sfArkLib                 url =  https://github.com/raboof/sfArkLib.git
sfarkxtc                 url =  https://github.com/raboof/sfarkxtc.git
spdlog                   url =  https://github.com/gabime/spdlog.git
sway                     url =  git@github.com:swaywm/sway.git
sway/subprojects/wlroots url =  git@github.com:swaywm/wlroots.git
tmux                     url =  git@github.com:tmux/tmux.git
transmission             url =  git@github.com:transmission/transmission.git
transmission             url =  https://github.com/transmission/dht
transmission             url =  https://github.com/transmission/libb64
transmission             url =  https://github.com/transmission/libevent
transmission             url =  https://github.com/transmission/libnatpmp
transmission             url =  https://github.com/transmission/libutp
transmission             url =  https://github.com/transmission/miniupnpc
uncrustify               url =  https://github.com/uncrustify/uncrustify.git
vifm-colors              url =  git@github.com:vifm/vifm-colors.git
wren                     url =  https://github.com/wren-lang/wren.git
xkb-switch               url =  https://github.com/grwlf/xkb-switch.git
yandex-disk-indicator    url =  https://github.com/slytomcat/yandex-disk-indicator.git
