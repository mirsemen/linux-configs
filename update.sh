#!/bin/bash

flist=`find . -maxdepth 1 -type d -path "./[^-]*" | sort | rg -v archive`;

for d in $flist; do
    cd $d
    echo -e "\e[31mUpdating \e[35m$d\e[0m"
    git pull --rebase --autostash --recurse-submodules
    cd -
done
